# Command line boilerplate code

Command linen utility boilerplate code

## Getting Started
The core purpose of this project is to boot-start the development of the service component by using this bare-bone project library.

## Prerequisites
- Ensure all pre-requisites are met. Refer to the shared folder `Process Catalog > Configuration Management > GitLab`
- Ensure your new service library is ready and you have initialized git repo

| Software  | Version       |
|-----------|:--------------|   
| Node      | 12.17.0 LTS   |
| Kubernetes| v1.18.2       |
| docker    | v19.0.3       |
| VS Code   | Latest        |
| yarn      | 1.22.4        |
| typescript| 3.9.3         |
> **NOTE:** Please note that the versions will keep on changing. 


## Built With
* [Skaffold](https://skaffold.dev/) - Local kubernetes development
* [Node](https://nodejs.org/en/) - Unddeline code framework
* [Docker](https://www.docker.com/) - Container build engine


## Authors
* **Muhammad Furqan** - *Initial work* - [Openfintechlab.com](https://openfintechlab.com)

